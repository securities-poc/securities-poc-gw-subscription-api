openapi: 3.0.0
info:
  title: Data Feed Subscription API
  description: This spec describes the API that manages internal microservices subscriptions to data feeds via the securities gateway.
  version: v1.0
tags:
  - name: Create Subscription
    description: ' '
  - name: Retrieve All Subscriptions
    description: ' '
  - name: Retrieve Subscription By Type and ID
    description: ' '
  - name: Cancel Subscription
    description: ' '
paths:
  /subscriptions:
    get:
      tags:
        - Retrieve All Subscriptions
      responses:
        '200':
          description: Retrieves all subscriptions of all types
          content:
            application/vnd.service.gateway.v1+json:
              schema:
                $ref: '#/components/schemas/AllSubscriptions'
        default:
          description: Error response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorMessageResponse'

    post:
      tags:
        - Create Subscription
      requestBody:
        content:
          application/vnd.service.gateway.v1+json:
            schema:
              $ref: '#/components/schemas/Subscription'
      responses:
        '201':
          description: Subscription successfully created
          headers:
            Location:
              description: uri link to created resource
              required: true
              schema:
                type: string
        default:
          description: Error response
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorMessageResponse'

  /subscriptions/{type}/{id}:
    get:
      tags:
        - Retrieve Subscription By Type and ID 
      responses:
        '200':
          description: ''
          content:
            application/vnd.service.gateway.v1+json:
              schema:
                $ref: '#/components/schemas/Subscription'
        default:
          description: Unauthorised Access
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorMessageResponse'
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
        - name: type
          in: path
          required: true
          schema:
            type: string

    delete:
      tags:
        - Cancel Subscription
      responses:
        '204':
          description: Subscription successfully cancelled
        default:
          description: Unauthorised Access
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ErrorMessageResponse'
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
        - name: type
          in: path
          required: true
          schema:
            type: string

servers:
  -
    url: 'http://{username}:{port}/{basePath}'
    description: 'DEV Server'
    variables:
      username:
        default: 'localhost'
        description: 'hostname'
      port:
        enum:
          - '7100'
          - '8080'
        default: '7100'
      basePath:
        default: ''


components:
  parameters:
    trait_secured_Authorization:
      description: The auth token for this request
      in: header
      name: Authorization
      required: true
      schema:
        type: string
  responses:
    trait_secured_401:
      description: Unauthorized
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/ErrorMessageResponse'
  schemas:
    SubscriptionType:
      type: string
      enum:
        - STOCK_LIVE_DATA
        - STOCK_PORTFOLIO
        - FX_LIVE_DATA

    Subscription:
      description: Base subscription
      type: object
      required:
        - id
        - creationDate
        - subscriptionType
        - subscriptionState
      properties:
        id:
          type: string
          minLength: 1
          maxLength: 18
        creationDate:
          type: string
          format: date
          example: '2019-05-07'
          pattern: '^([0-9]{4})(-?)(1[0-2]|0[1-9])\2(3[01]|0[1-9]|[12][0-9])'
        subscriptionType:
          $ref: '#/components/schemas/SubscriptionType'
        subscriptionState:
          type: string
          enum:
            - ACTIVE
            - DEACTIVATED
            - CANCELLED

    PortfolioSubscription:
      allOf:
        - $ref: '#/components/schemas/Subscription'
        - type: object
          required:
            - name
          properties:
            name:
              type: string

    PortfolioSubscriptions:
      type: object
      properties:
        subscriptions:
          type: array
          minItems: 0
          maxItems: 100
          items:
            $ref: '#/components/schemas/PortfolioSubscription'

    ForexSubscription:
      allOf:
        - $ref: '#/components/schemas/Subscription'
        - type: object
          required:
            - currencyPairs
          properties:
           currencyPairs:
             type: array
             minItems: 0
             maxItems: 50
             items:
              type:  string

    ForexSubscriptions:
      type: object
      properties:
        subscriptions:
          type: array
          minItems: 0
          maxItems: 100
          items:
            $ref: '#/components/schemas/ForexSubscription'

    AllSubscriptions:
      type: object
      properties:
        portfolio:
          type: array
          minItems: 0
          maxItems: 100
          items:
            $ref: '#/components/schemas/PortfolioSubscriptions'
        forex:
          type: array
          minItems: 0
          maxItems: 100
          items:
            $ref: '#/components/schemas/ForexSubscriptions'

    ErrorMessageResponse:
      description: Error response message
      properties:
        code:
          minLength: 1
          type: string
        correlationId:
          minLength: 1
          type: string
        description:
          minLength: 1
          type: string
      required:
        - correlationId
        - code
        - description
      type: object






